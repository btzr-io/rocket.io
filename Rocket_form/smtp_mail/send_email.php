<?php

// Form values
require_once('smtp_data.php');
require_once('../My_forms/values.php');
require_once('php_mailer/PHPMailerAutoload.php');

$mail = new PHPMailer;
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;             
$mail->Debugoutput = 'html';
$mail->IsSMTP();
//Set the hostname of the mail server
$mail->Host = $SMTP_server;
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = $SMTP_port;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = $SMTP_secure;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = $SMTP_user;
$mail->Password = $SMTP_password;        
//Set who the message is to be sent from
$mail->setFrom($SMTP_email , 'Contacto Web');
//Set an alternative reply-to address : 
$mail->addReplyTo('sisede4co@mail.com' , 'reply');
//Set who the message is to be sent to
$mail->addAddress($SMTP_email , 'Contacto Web');
//Set the subject line
$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
$mail->isHTML(true);// Set email format to HTML         
$mail->Subject = 'Contacto web';

$mail->Body = '<div style="background:#d5d5d5; font-size:1.25em;  width:100%;  padding:50px 0; font-family: Arial, sans-serif;word-wrap: break-word;"><div style="background:#fff; width:380px; padding:30px 50px; margin:auto; border-radius:5px;"><h1 style="font-family: Montserrat, sans-serif; color:#555; border-bottom:1px solid #ddd; font-weight:bold;">Contacto:</h1><p style="background:#0077c0; color:#fff; padding:7px;"><span style="margin-left:10px;">Nombre:</span> <span style="margin-left:10px;">'.$Rocket_name.'<span></p><p style="color:#585858;"><span style="margin-left:10px;">Email:</span> <span style="padding:5px 10px; color:#009cde; margin-left:10px;">'.$Rocket_email.'</span></p><div style="background:#eee;"><p style="background:#555; color:#fff; padding:5px;"><span style="margin-left:10px;">Mensaje:</span></p><p style="padding:20px 10px;word-wrap: break-word;">'.$Rocket_message.'</p></div><br><p style="color:#757575; text-align:center;border-top:1px solid #ddd; padding-top:10px">www.sisede.com</p></div></div>';
$mail->AltBody = 'Name:'.$Rocket_name.', Email: '.$Rocket_email.', Mensaje: '.$Rocket_message;


if(!$mail->send()) {echo 'Message could not be sent.';} 

else {
$url = 'http://contact22-btzr.rhcloud.com/Rocket_form/My_forms/contact_us/contact_form.php';
echo 'Message has been sent';
header('Location: '.$url);
die();
}
?>



